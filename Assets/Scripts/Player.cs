﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    public Tile[] board = new Tile[Constants.NumTiles];
    public Node parent;
    public List<Node> childList = new List<Node>();
    public int type;//Constants.MIN o Constants.MAX
    public double utility;
    public double alfa;
    public double beta;

    public Node(Tile[] tiles)
    {
        for (int i = 0; i < tiles.Length; i++)
        {
            this.board[i] = new Tile();
            this.board[i].value = tiles[i].value;
        }

    }    

}

public class Player : MonoBehaviour
{
    public int turn;    
    private BoardManager boardManager;

    void Start()
    {
        boardManager = GameObject.FindGameObjectWithTag("BoardManager").GetComponent<BoardManager>();
    }
       
    /*
     * Entrada: Dado un tablero
     * Salida: Posición donde mueve  
     */
    public int SelectTile(Tile[] board)
    {        
             
        //Generamos el nodo raíz del árbol (MAX)
        Node root = new Node(board);
        root.type = Constants.MAX;
        int tile_to_move = GenerateTree(root, 2);

        return tile_to_move;

    }



    /*
     * Obtine el movimiento más optimo acorde a un listado de valores
     * generados por el algoritmo MINIMAX
     */

    public int GenerateTree(Node root, int depth){
        int movimiento = 0;
        List<int> selectableTiles = boardManager.FindSelectableTiles(root.board, turn); //Lista de posibles movimientos (nodos del arbol)

        //Genera un listado de valores siguiendo el sistema MINIMAX hasta una profundidad determinada y devuelve los valores del nodo adyacente al actual
        List<int> nodesValues = SpreadNodes(root, depth, 0); 

        if(nodesValues.Count>0){

            List<int> bestMoves = new List<int>();
            int maxValue = getMaxValue(nodesValues); //Obtenemos el mayor valor para conocer la mejor jugada posible para MAX

            //Contamos la cantidad de las jugadas clasificads como las mejores posibles en este movimiento
            for(int i=0; i<nodesValues.Count;i++){
                if(nodesValues[i]==maxValue){bestMoves.Add(i);} //Guardamos el indice que corresponde a la tile a la que haya que realizar el movimiento
            }

            //PrintNodesValues(bestMoves);
            //Se elige el mejor movimiento, en caso de que hayan varios movimientos clasificados como el mejor posible, se obtiene uno de ellos aleatoriamente
            movimiento = bestMoves[0];
            if(bestMoves.Count>1){
                movimiento = bestMoves[Random.Range(0,bestMoves.Count)];
            }

        } //No haymovimientos posibles

        //Debug.Log("Movimiento: "+movimiento);
        return selectableTiles[movimiento]; //Se retorna la tile optima a la que moverse

    }




    /*
     * Algoritmo recursivo implementado con MINIMAX, que se llama recursivamente hasta una profundidad determinada
     * devolviendo un listado con los posibles nodos
     */
    public List<int> SpreadNodes(Node n_origin, int depth, int c_depth){
        
        List<int> nodesValues = new List<int>();  //Listado de nodos posibles en función del original
        c_depth++; //Se añade 1 al contador de prfoundidad de la iteración
        

            List<int> selectableTiles = boardManager.FindSelectableTiles(n_origin.board, turn); //Lista de posibles movimientos (nodos del arbol)
            foreach(int tile in selectableTiles){
                //por cada posible movimiento se genera un nodo
                Node n = new Node(n_origin.board); //Se le establece al nodo un tablero (estado actual para dicho nodo)
                n_origin.childList.Add(n); //Se le asocia al nodo padre este como hijo
                n.parent = n_origin; // Y viceversa, se asocia como padre el nodo origen

                //Si la iteracion anterior es MIN, los nodos de esta iteración serán MAX (y viceversa)
                if(n_origin.type==Constants.MIN){
                    n.type = Constants.MAX;
                } else{ n.type = Constants.MIN;}

                //Aplico un movimiento, generando un nuevo tablero con ese movimiento
                boardManager.Move(n.board, tile, turn);

                //si queremos imprimir el nodo generado (tablero hijo)
                //boardManager.PrintBoard(n.board);

                int value = 0; //Valor del nodo

                //Se guardan el número de piezas que cambiarian y se suman a las piezas correspondientes al jugador del turno actual y se restan al jugador contrario
                int swp = boardManager.FindSwappablePieces(n.board, tile, n.type).Count; 
                int pieces = boardManager.CountPieces(n.board, n.type); 

                int t_pieces = pieces+swp; //Cantidad de piezas que quedarían al jugador del turno del nodo actual
                
                int r_pieces = boardManager.CountPieces(n.board, -n.type);
                r_pieces = r_pieces - swp;  //Cantidad de piezas que quedarian del jugador del turno contrario al del nodo actual


                //En función del turno en el que se encuentre el nodo anterior se almacena 
                //en el valor del nodo, la cantidad de piezas blancas que quedarían despues de la jugada
                switch(n.type){
                    case Constants.MIN:
                        //Debug.Log("piezas BLANCAS: "+t_pieces+ ", negras "+r_pieces);
                        value = t_pieces;
                    break;
                    case Constants.MAX:
                        //Debug.Log("piezas negras: "+t_pieces+ ", BLANCAS "+r_pieces);
                        value = r_pieces;
                    break;
                }


                ///////////////////////
                //Se llama recursivamente al algoritmo para exapndir el arbol de nodos
                //////////////////////
                if(c_depth<depth){
                    List<int> nextBranch = SpreadNodes(n, depth, c_depth); 
                    int nextBranchType = -n.type; // Obtenermos si la rama siguiente (al igual que la anterior a la actual) es MIN o MAX

                    //Dependiendo de si es MIN/MAX, se sustituirá el valor del nodo actual, por el menor o mayor valor de la siguiente iteración de ramas 
                    if(nextBranch.Count>0){
                        switch(nextBranchType){
                            case Constants.MIN:
                                value = getMinValue(nextBranch);
                            break;
                            case Constants.MAX:
                                value = getMaxValue(nextBranch);
                            break;
                        }
                    } //Si no hay movimientos posibles se ignora

                 }


                n.utility = value; //Se asocia a cada nodo su valor 
                nodesValues.Add((int)n.utility); //Se añade un listado de valores por cada iteración del algoritmo

            }

        //PrintNodesValues(nodesValues);
        Debug.Log("");

        return nodesValues;
    }




    /*
     * Muestra la por consola los valores almacenados en un nodo
     */
    public void PrintNodesValues(List<int> values){
        string s="[";
        foreach(int val in values){
            s += val+" ";
        }
        s+="]";

        Debug.Log(s);
    }



    /*
     * Obtiene el valor mínimo de una lista
     */
    public int getMinValue(List<int> values){
        int result = values[0];
        for(int i=1; i<values.Count; i++){
            if(values[i]<result){result = values[i];}
        }

        return result;
    }


    /*
     * Obtiene el valor máximo de una lista
     */
    public int getMaxValue(List<int> values){
        int result = values[0];
        for(int i=1; i<values.Count; i++){
            if(values[i]>result){result = values[i];}
        }

        return result;
    }



}

